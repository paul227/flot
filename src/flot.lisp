(defpackage :flot
  (:use :common-lisp :flot.read-stream)
  (:export
   :read-stream-on
   :next-match-for
   :at-end-p
   :next
   :reset
   :get-position
   :peek
   :peek-for
   :set-to-end
   :up-to
   :up-to-end
   :contents))

(in-package :flot)
