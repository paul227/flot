(asdf:defsystem :flot
  :serial t
  :components ((:file "flot-stream")
	       (:file "positionable-stream")
	       (:file "read-stream")
	       (:file "flot")))
