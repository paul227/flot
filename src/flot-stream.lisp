(defpackage :flot.stream
  (:use :common-lisp)
  (:export
   :flot-stream
   :at-end-p
   :next
   :do-each
   :contents
   :next-match-for))

(in-package :flot.stream)

(defclass flot-stream ()
  ((contents :initarg :on
             :initform (error "Must provide a value to stream on.")
             :reader contents
             :documentation "return all the objects in the collection.")))

(defgeneric at-end-p (flot-stream)
  (:documentation "Answer if cannot access any more objects"))

(defgeneric next (flot-stream)
  (:documentation "return the next object accessible in the collection."))

(defgeneric next-match-for (flot-stream value)
  (:documentation "Access the next object and return whether it is equal to the argument, \"value\""))

(defgeneric do-each (flot-stream func)
  (:documentation "Evaluate the argument, \"func\", for each of the remaining elements that can be accessed in the collection."))
