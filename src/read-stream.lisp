(defpackage :flot.read-stream
  (:use
   :common-lisp
   :flot.positionable-stream)
  (:export
   :read-stream-on
   :next-match-for
   :at-end-p
   :next
   :reset
   :get-position
   :peek
   :peek-for
   :set-to-end
   :up-to
   :up-to-end
   :contents))

(in-package :flot.read-stream)

(defclass read-stream (positionable-stream)
  ((read-stream-limit :initarg :limit-stream
		      :reader read-stream-limit))
  (:documentation "is a concrete subclass of positionable-stream that represents
 an accessor that can only read elements form its collection."))

(defmethod initialize-instance :after ((r read-stream) &key)
  (with-slots (contents read-stream-limit is-empty-p) r
    (setf read-stream-limit (1- (length contents)))
    (setf is-empty-p (= 0 (length contents)))))

(defun read-stream-on (value)
  (make-instance 'read-stream :on value))

(defmethod at-end-p ((r read-stream))
  (with-slots (read-stream-limit) r
    (>= (get-position r) read-stream-limit)))

(defmethod next ((r read-stream))
  (if (not (at-end-p r))
      (with-slots (contents position) r
        (setf position (1+ position))
        (elt contents position))
      nil))

(defmethod reset ((r read-stream))
  (with-slots (position) r
    (setf position -1)
    r))

(defmethod peek ((r read-stream))
  (when (not (at-end-p r))
    (with-slots (contents position) r
      (elt contents (1+ position)))))

(defmethod peek-for ((r read-stream) value)
  (when (eql (peek r) value)
    (with-slots (position) r
      (setf position (1+ (get-position r))))))

(defmethod set-to-end ((r read-stream))
  (with-slots (position read-stream-limit) r
    (setf position read-stream-limit)))

(defmethod up-to ((r read-stream) value)
  (do ((n (next r) (next r))
       (result '() (push  n result)))
      ((or (eql n value) (at-end-p r)) (reverse result))))

(defmethod up-to-end ((r read-stream))
  (let ((start (+ 1 (get-position r)))
	(position (+ 1 (read-stream-limit r))))
    (subseq (contents r) start position))) 

(defmethod skip ((r read-stream) n)
  (with-slots (position) r
    (setf position (+ (get-position r) n 1))
    (when (at-end-p r)
      (set-to-end r))))

(defmethod next-match-for ((r read-stream) value)
  (eql (next r) value))

