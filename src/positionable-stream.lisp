(defpackage :flot.positionable-stream
  (:use
   :common-lisp
   :flot.stream)
  (:export
   :contents
   :get-position
   :is-empty-p
   :peek
   :peek-for
   :positionable-stream
   :reset
   :reverse-contents
   :set-to-end
   :skip
   :skip-to
   :up-to
   :up-to-end))

(in-package :flot.positionable-stream)

(defclass positionable-stream (flot-stream)
  ((position :initform -1
             :reader get-position)
   (is-empty-p :reader is-empty-p))
  (:documentation "A positionable-stream is a subclass of flot-stream. 
It provide additional methods appropriate to streams that can reposition their position references, but, 
it is an abstract class because it does not provide an implementation of the inherited method next. 
The implementation of theses generics is left to the subclass of positionable-stream: read-stream."))

(defgeneric peek (positionable-stream)
  (:documentation "Return the next element in the collection (as in the method next), but do not change the position reference. Return nil if reference position is at the end."))

(defgeneric peek-for (positionable-stream value)
  (:documentation "Determine the response of the method peek. If it is the same as the argument, \"value\", then increment the position reference and return true. Otherwise return false and do not change the position reference."))

(defgeneric up-to (positionable-stream value)
  (:documentation "Answer a collection of elements starting with the next element accessed in the collection, and up to, not inclusive of, the next element that is equal to \"value\". If \"value\" is not in the collection, answer the entire rest of the collection."))

(defgeneric up-to-end (positionable-stream)
  (:documentation "Answer a collection of elements starting with the next element accessed in the collection, and up to the end of the stream."))

(defgeneric reverse-contents (positionable-stream)
  (:documentation "Answer a copy  of the receiver's contents in reverse order."))

(defgeneric reset (positionable-stream)              
  (:documentation "Set the reference position to the beginning of the collection."))

(defgeneric set-to-end (positionable-stream)
  (:documentation "Set the reference position to the end of the collection."))

(defgeneric skip (positionable-stream n)
  (:documentation "Set the reference position to be the current position plus the argument, \"n\", possibly adjusting the result so as the remain within the bounds of the collection."))

(defgeneric skip-to (positionable-stream value)
  (:documentation "Set the reference position to be past the next occurence of the argument, \"value\", in the collection. Answer whether such an occurrence existed."))

